# Main developper

- Luc Didry, <https://fiat-tux.fr>, <https://luc.frama.io>, on [Diaspora*](https://framasphere.org/people/b13eb6b0beac0131e7e32a0000053625), on [Mastodon](https://framapiaf.org/@framasky)

# Contributor

- ValVin, <https://blog.valvin.fr>, on [Diaspora*](https://framasphere.org/people/1f5811905567013347142a0000053625), on [Twitter](https://twitter.com/_valvin), on [Mastodon](https://framapiaf.org/@valvin)
